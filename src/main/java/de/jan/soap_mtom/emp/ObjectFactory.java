//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.10.02 at 04:07:02 PM IST 
//


package de.jan.soap_mtom.emp;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.innova.models.soap.emp package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.innova.models.soap.emp
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetShipmentRequest }
     * 
     */
    public GetShipmentRequest createGetShipmentRequest() {
        return new GetShipmentRequest();
    }

    /**
     * Create an instance of {@link GetShipmentResponse }
     * 
     */
    public GetShipmentResponse createGetShipmentResponse() {
        return new GetShipmentResponse();
    }

    /**
     * Create an instance of {@link TransferObject }
     * 
     */
    public TransferObject createShipment() {
        return new TransferObject();
    }

}
