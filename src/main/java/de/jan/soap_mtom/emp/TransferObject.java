package de.jan.soap_mtom.emp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlType;
import java.awt.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transferobject", propOrder = {
        "name",
        "image"
})
public class TransferObject {

    private String name;
    @XmlMimeType("image/jpeg")
    private Image image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
