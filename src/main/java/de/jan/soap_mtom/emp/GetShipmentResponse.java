package de.jan.soap_mtom.emp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "transferObject"
})
@XmlRootElement(name = "getShipmentResponse")
public class GetShipmentResponse {

//    protected CTGetShipmentResponse shipmentResponse;

    private TransferObject transferObject;

    public TransferObject getTransferObject() {
        return transferObject;
    }

    public void setTransferObject(TransferObject transferObject) {
        this.transferObject = transferObject;
    }

    //    public CTGetShipmentResponse getShipmentResponse() {
//        return shipmentResponse;
//    }
//
//    public void setShipmentResponse(CTGetShipmentResponse shipmentResponse) {
//        this.shipmentResponse = shipmentResponse;
//    }
}
