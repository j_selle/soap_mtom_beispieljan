package de.jan.soap_mtom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchDataAutoConfiguration;


@SpringBootApplication
public class SoapMtomApplication {

    public static void main(String[] args) {
        SpringApplication.run(SoapMtomApplication.class, args);
    }

}
