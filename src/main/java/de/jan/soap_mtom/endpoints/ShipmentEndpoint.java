package de.jan.soap_mtom.endpoints;



import de.jan.soap_mtom.emp.GetShipmentRequest;
import de.jan.soap_mtom.emp.GetShipmentResponse;
import de.jan.soap_mtom.emp.TransferObject;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Endpoint
public class ShipmentEndpoint {

    private static final String NAMESPACE_URI = "http://jan.de/soap_mtom/emp";
    private static final String DATEI_ID = "1";
    private static final String DATEI_NAME = "sample.pdf";
//    private static String filePath = "sample.pdf";
//    private static String filePath = "1.jpeg";
    private static String filePath = "D:\\Projekte\\intern\\soap_mtom - own\\src\\main\\resources\\1.jpeg";



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getShipmentRequest")
    @ResponsePayload
    public GetShipmentResponse getShipment(@RequestPayload GetShipmentRequest request) {
        TransferObject transferObject = new TransferObject();
        File file = new File(filePath);
        System.out.println();
        try {
            transferObject.setName("test");
            transferObject.setImage(ImageIO.read(new File(filePath)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        GetShipmentResponse response = new GetShipmentResponse();
        response.setTransferObject(transferObject);
        return response;
    }


    public byte[] getFile(String fileName) {
        try(InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(fileName)) {
            byte[] targetArray = new byte[inputStream.available()];
            inputStream.read(targetArray);
            return targetArray;
        } catch(IOException ex) {
            throw new IllegalStateException("Can not read file from resources", ex);
        }
    }

//    public File getPDF(String filePath) {
//        File file = new File(filePath);
//    }


}
